<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>

	<script src="public-resource/node_modules/core-js/client/shim.min.js"></script> 
	<script src="public-resource/node_modules/zone.js/dist/zone.js"></script> 
	<script src="public-resource/node_modules/reflect-metadata/Reflect.js"></script> 
	<script src="public-resource/node_modules/systemjs/dist/system.src.js"></script> 
	<script src="public-resource/app/systemjs.config.js"></script> 

</head>
<body>
	<my-app>Loading AppComponent content here ...</my-app>
</body>
</html>