package net.suby.wise.login.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import net.suby.wise.login.dao.mybatis.UserDataMapper;

@Controller
public class LoginController {
//	@Autowired
//    private UserReposiroty userReposiroty;

	@Autowired
	private UserDataMapper userDataMapper;
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login(@RequestParam(value = "error", required = false) String error){
		if (error != null) {
//			model.addObject("error", "Invalid username and password!");
			
		  }
		return "login";
	}
	
	@RequestMapping(value="/login/check", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> loginCheck(HttpServletRequest request, @RequestParam HashMap<String, String> paramMap){
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("ok", "ok222");
		return resultMap;
	}

	@RequestMapping(value="/403", method=RequestMethod.GET)
	public String E403(){
		return "403";
	}
	
	@RequestMapping(value="/admin", method=RequestMethod.GET)
	public String admin(){
		return "admin";
	}
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public String index(){
		return "index";
	}

	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpServletResponse response){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
		return "redirect:/login?logout";
	}
}
